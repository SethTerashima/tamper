import sys
import os
import math
from collections import defaultdict

BLOCKSIZE = 16

class Node:
	def __init__(self, filename, parent):
		self.hash = ""
		self.filename = filename
		self.parent = parent

def findKnownPlaintext(ptFilename, encryptedDirectory, matchToDepth = 0):
	ptFilename = os.path.abspath(ptFilename)
	ptPath = ptFilename
	
	for i in range(max(matchToDepth, 1)):
		ptPath = os.path.dirname(ptPath)
	
	#print "Scanning plaintext directory %s..." % ptPath
	ptF2N, foo = scanDirectory(ptPath, False)

	#print "Scanning encrypted directory..."
	foo, ctH2N = scanDirectory(encryptedDirectory, True)

	ptNode = ptF2N[ptFilename]
	#print "Plaintext hash: %s" % ptNode.hash
	if ptNode == None:
		return

	matches = []
	ptAncestor = ptNode

	for i in range(matchToDepth):
		if ptAncestor.parent == None:
			sys.stderr.write("Error: Directory not scanned to depth %d" % matchToDepth)
			exit()
		ptAncestor = ptAncestor.parent
	
	#print "Plaintext dir hash: %s" % ptAncestor.hash
	#print "Hash of target file (%s): %s" % (ptNode.filename, ptNode.hash)
	#print "Hash of target directory (%s): %s" % (ptAncestor.filename, ptAncestor.hash)

	# TODO: It would be faster to find matching ancestory directories at the correct
	# detph, and then walk down the tree from there (rather than starting at candidate
	# files and walking up the tree). The former approach immediately eliminates false
	# positives.
	for potentialTarget in ctH2N[ptNode.hash]:
		ctAncestor = potentialTarget
		#print "Target hash: %s" % potentialTarget.hash
		for i in range(matchToDepth):
			if ctAncestor.parent == None:
				break
			ctAncestor = ctAncestor.parent	
		if ptAncestor.hash == ctAncestor.hash:
			matches.append(potentialTarget.filename)
	return matches
	
def scanDirectory(rootDir, encrypted = True):
	root = Node(rootDir, None)
	filenameToNode = {}
	hashToNode = defaultdict(list)
	__buildHashTable(rootDir, filenameToNode, hashToNode, root, encrypted)
	return (filenameToNode, hashToNode)

def __roundTo(n, blocksize):
	return int(blocksize * math.ceil(float(n) / blocksize))

def __buildHashTable(filename, filenameToNode, hashToNode, node, encrypted):
	if not os.path.lexists(filename):
		sys.stderr.write("Error: invalid path %s\n" % filename)
		exit()

	filenameToNode[filename] = node
	
	if os.path.isfile(filename) or os.path.islink(filename):
		#print "F %s" % filename
		info = os.lstat(filename)
		if encrypted:
			filenameLength = len(filename)
			fileSize = info.st_size
		else:
			filenameLength = BLOCKSIZE + __roundTo(len(filename), BLOCKSIZE) 
			fileSize = 4096 * 2 + __roundTo(__roundTo(info.st_size, BLOCKSIZE), 4096)
		rv = str((info.st_mode, fileSize)) #, filenameLength))
		#rv = "X"
		hashToNode[rv].append(node)
		node.hash = rv
		return rv
	elif os.path.isdir(filename):
		#print "D %s" % filename
		rv = []
		for f in os.listdir(filename):
			child = Node(filename + "/" + f, node)
			rv.append(__buildHashTable(child.filename, filenameToNode, hashToNode, child, encrypted))
		rv.sort()
		rv = "[" + ",".join(rv) + "]"
		node.hash = rv
		hashToNode[rv].append(node)
		return rv
	else:
		print "I don't know what to do with %s" % filename
		exit()
		return None

if __name__ == "__main__":
	if len(sys.argv) < 3 or len(sys.argv) > 4:
		print "Usage: search.py known-plaintext encrypted-directory [match-depth]"
		print """Prints a list of files in encrypted-directory (and its sub-directories) that
		are possible ciphertexts for known-plaintext. In order for a file to be a
		possible ciphertext, its parent directories must be possible matches for the
		parent directories of the known-plaintext, up to the specified depth (which
		defaults to 1)."""
		exit()

	ptFile = sys.argv[1]
	encryptedDirectory = sys.argv[2]
	if len(sys.argv) == 4:
		matchDepth = int(sys.argv[3])
	else:
		matchDepth = 1

	if not os.path.isfile(ptFile):
		sys.stderr.write("File %s does not appear to exist (or is a directory)." % ptFile)
		exit()

	if not os.path.isdir(encryptedDirectory):
		sys.stderr.write("Path %s does not exist (or is not a directory)." % encryptedDirectory)
		exit()

	for match in findKnownPlaintext(ptFile, encryptedDirectory, matchDepth):
		print match

