This project is a PoC that exploits known weaknesses in the eCryptfs encrypted file system. The two weaknesses in question, which are particularly dangerous in combination, are:
    1. eCryrptfs leaks file metadata, such as directory strutures, file permissions, and coarse-grained information about file sizes.
    2. eCryptfs does not use authenticated encryption. This means an attacker can tamper with a ciphertext, and this tampering will not be automatically detected by eCryptfs.

The Tamper project contains two components:
   1. The ability to search for ciphertexts corresponding to a known plaintext. For example, if you suspect that an encrypted directory contains an installation of the Tor Browser somewhere inside, you can verify this with a high degree of accuracy (encryption not withstanding).
   2. Code for tampering with ciphertexts to induce semantically meaningful changes in the resulting plaintext. This code targets bash scripts in particular. It tampers with the encrypted bash script to inject arbitrary commands in the corresponding plaintext. This is made possible by the lack of authentication; the CBC-AES encryption prevents an attacker from learning information about a plaintext (aside from leaked metadata), but does not prevent an attacker who already knows some information from mounting this attack.

For example, suppose Bob has the Tor Browser bundle installed in an encrypted eCryptfs directory on, say, a flash drive or some type of remote storage. If Eve can access this storage, she can confirm that the Tor Browser Bundle is installed, and then tamper with the (encrypted) startup script. The next time Bob decrypts and executes this script, it will send Bob's private SSH key to Eve before running the Tor Browser as usual. Bob is left none the wiser unless he actually inspects the script file.

Due to the nature of CBC encryption and the way shell commands are injected into ciphertexts, there is some (small but non-negligible) probability that the script will crash, alerting Bob that something is wrong.
